<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy
 */

namespace Mediarise\SberPayment;

use Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Illuminate\Support\Facades\Log;
use Mediarise\SberPayment\Exceptions\SberPaymentServiceException;

class SberPaymentService
{
    /**
     * @var SoapWrapper
     */
    protected SoapWrapper $soapWrapper;
    protected ?string $error = null;
    protected ?int $errorCode;

    const STATUS_PROCESSING = 0;
    const STATUS_SUCCEEDED = 1;
    const STATUS_CANCELED = 2;

    /**
     * SberService constructor.
     *
     * @param SoapWrapper $soapWrapper
     * @throws ServiceAlreadyExists
     */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;

        $this->soapWrapper->add('Sber', function ($service) {
            $service
                ->wsdl(config('sber-payment.soap.wsdl'))
                ->trace(true)
                ->customHeader(new WsseAuthHeader(config('sber-payment.soap.username'), config('sber-payment.soap.password')));
        });
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function getPayLink(array $data)
    {
        $amount = $data['amount'] * 100;
        $root = new \SimpleXMLElement('<root/>');
        $order = $root->addChild('order');
        $order->addAttribute('description', $data['description']);
        $order->addAttribute('amount', $amount);
        $order->addAttribute('merchantOrderNumber', $data['merchantOrderNumber']);

        $order->addChild('returnUrl', $data['successUrl']);
        $order->addChild('failUrl', $data['failUrl']);

        $orderBundle = $order->addChild('orderBundle');

        $customerDetails = $orderBundle->addChild('customerDetails');
        $customerDetails->addChild('phone', $data['phone']);
        $customerDetails->addChild('fullName', $data['fullName']);

        $cartItems = $orderBundle->addChild('cartItems');
        $items = $cartItems->addChild('items');
        $items->addAttribute('positionId', 1);
        $items->addChild('name', $data['name']);
        $quantity = $items->addChild('quantity ', 1);
        $quantity->addAttribute('measure', 'услуга');
        $items->addChild('itemCode', 1);

        if (!empty($data['bonus'])) {
            $discount = $items->addChild('discount');
            $discount->addChild('discountType', 'оплачено бонусами');
            $discount->addChild('discountValue', $data['bonus']);
        }

        $items->addChild('itemAmount', $amount);
        $items->addChild('itemPrice', $amount);

        $data = new \SoapVar($order->asXML(), XSD_ANYXML);
        try {
            $response = $this->soapWrapper->call('Sber.registerOrder', [$data]);
            if ($response->errorCode) {
                throw new SberPaymentServiceException($response->errorMessage, 400);
            }
            $client = $this->soapWrapper->client('Sber', function ($client) {
                Log::info('getPayLink. Request - ', [$client->getLastRequest()]);
                Log::info('getPayLink. Response - ', [$client->getLastResponse()]);
            });
            return $response;
        } catch (\Exception $e) {
            Log::error('getPayLink. ERROR - ', [$e->getTraceAsString()]);

            $client = $this->soapWrapper->client('Sber', function ($client) {
                Log::error('getPayLink. ERROR. Request - ', [$client->getLastRequest()]);
                Log::error('getPayLink. ERROR. Response - ', [$client->getLastResponse()]);
            });
            throw new SberPaymentServiceException($e->getMessage(), 400);
        }

    }

    /**
     * @param $orderId
     * @return mixed
     * @throws \Exception
     */
    public function getOrderStatus($orderId): array
    {
        $root = new \SimpleXMLElement('<root/>');
        $order = $root->addChild('order');
        $order->addAttribute('orderId', $orderId);

        $data = new \SoapVar($order->asXML(), XSD_ANYXML);
        try {
            $response = $this->soapWrapper->call('Sber.getOrderStatus', [$data]);
            if ($response->errorCode) {
                throw new SberPaymentServiceException($response->errorMessage, 400);
            }
            $client = $this->soapWrapper->client('Sber', function ($client) {
                Log::info('SBER. getOrderStatus. Request - ', [$client->getLastRequest()]);
                Log::info('SBER. getOrderStatus. Response - ', [$client->getLastResponse()]);
            });

            $isPayed = false;
            switch ($response->orderStatus) {
                case 2:
                    $statusCode = self::STATUS_SUCCEEDED;
                    $isPayed = true;
                    break;
                case 0:
                case 1:
                case 5:
                    $statusCode = self::STATUS_PROCESSING;
                    break;
                case 3:
                case 4:
                default:
                    $statusCode = self::STATUS_CANCELED;
                    break;
            }

            return [
                'status' => trans('payment::statuses.' . $response->orderStatus, [], 'ru'),
                'statusCode' => $statusCode, 'isPayed' => $isPayed
            ];
        } catch (\Exception $e) {
            Log::error('getOrderStatus. ERROR - ', [$e->getTraceAsString()]);

            $client = $this->soapWrapper->client('Sber', function ($client) {
                Log::error('getOrderStatus. ERROR. Request - ', [$client->getLastRequest()]);
                Log::error('getOrderStatus. ERROR. Response - ', [$client->getLastResponse()]);
            });
            throw new SberPaymentServiceException($e->getMessage(), 400);
        }

    }

    /**
     * @return String|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return Int|null
     */
    public function getErrorCode(): ?int
    {
        return $this->errorCode;
    }
}
