<?php

namespace Mediarise\SberPayment;

use Illuminate\Support\ServiceProvider;

class SberPaymentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../ru/translations', 'statuses');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $configPath = __DIR__.'/../config/sber-payment.php';
        $this->mergeConfigFrom($configPath, 'sber-payment');
        $this->app->singleton('Mediarise\SberPayment\SberPaymentService');
    }
}
