<?php
/**
 * Created by PhpStorm.
 * User: Vitaliy
 */

namespace Mediarise\SberPayment;

class WsseAuthHeader extends \SoapHeader
{

    const WSS_NS = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';

    /**
     * WsseAuthHeader constructor.
     * @param $user
     * @param $password
     */
    function __construct($user, $password)
    {
        $auth = new \stdClass();
        $auth->Username = new \SoapVar($user, XSD_STRING, NULL, self::WSS_NS, NULL, self::WSS_NS);
        $auth->Password = new \SoapVar($password, XSD_STRING, NULL, self::WSS_NS, NULL, self::WSS_NS);

        $usernameToken = new \stdClass();
        $usernameToken->UsernameToken = new \SoapVar($auth, SOAP_ENC_OBJECT, NULL, self::WSS_NS, 'UsernameToken', self::WSS_NS);

        $security_sv = new \SoapVar(
            new \SoapVar($usernameToken, SOAP_ENC_OBJECT, NULL, self::WSS_NS, 'UsernameToken', self::WSS_NS), SOAP_ENC_OBJECT, NULL, self::WSS_NS, 'Security', self::WSS_NS);

        parent::__construct(self::WSS_NS, 'Security', $security_sv, true);
    }

}
