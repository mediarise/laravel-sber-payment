<?php

return [
    'soap' => [
        'url' => env('SOAP_SBER_URL', 'https://3dsec.sberbank.ru/payment/webservices/merchant-ws'),
        'wsdl' => env('SOAP_SBER_WSDL', 'https://3dsec.sberbank.ru/payment/webservices/merchant-ws?wsdl'),
        'username' => env('SOAP_SBER_USERNAME', ''),
        'password' => env('SOAP_SBER_PASSWORD', ''),
    ]
];
